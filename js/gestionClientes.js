function getClientes() 
{
  var url = "https://services.odata.org/V4/Northwind/Northwind.svc/Customers";
  var request = new XMLHttpRequest();


  request.onreadystatechange = function () {
  if (this.readyState ==4 && this.status == 200) {
      console.table(JSON.parse(request.responseText).value);
      clientesObtenidos = request.responseText;
   }
  }
  request.open("GET", url, true);
  request.send();
}

function procesarClientes() {
    var rutaBandera = "https://https://www.countries-ofthe-world.com/flags-normal/flag-of-"
    var JSONClientes = JSON.parse(clientesObtenidos);
    //alert(JSONProductos.value[0].ProductName);
    var divTabla = document.getElementById("divTablaClientes");
    var tabla = document.createElement("table");
    var tbody = document.createElement("tbody");
  
    tabla.classList.add("table");
    tabla.classList.add("table-stripped");
  
    for (var index = 0; index < JSONProductos.length; index++) {
  
      var nuevaFila = document.createElement("tr");
  
      var columnaNombre = document.createElement("td");
      columnaNombre.innerText = JSONClientes.value[index].ContactName;
  
      var columnaCiudad = document.createElement("td");
      columnaCiudad.innerText = JSONClientes.value[index].City;
  
      var columnaBandera = document.createElement("td");
      var imgBandera = document.createElement("img");
      imgBandera.classList.add("flag");

      if(JSONClientes.value[i].Country == "UK") {

        imgBandera.src = rutaBandera + "United-Kingdom.png";

      } else {

        imgBandera.src = rutaBandera + JSONClientes.value[index].Country + ".png";
      }

      columnaBandera.appendChild(imgBandera);
  
      nuevaFila.appendChild(columnaNombre);
      nuevaFila.appendChild(columnaCiudad);
      nuevaFila.appendChild(columnaBandera);
  
      tbody.appendChild(nuevaFila);
    }
  
    tabla.appendChild(tbody);
    divTabla.appendChild(tabla);
  }
