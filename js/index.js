function getProductos() 
{
  
  var url = "https://services.odata.org/V4/Northwind/Northwind.svc/Products";
  var request = new XMLHttpRequest();

  
  request.onreadystatechange = function () {
  if (this.readyState ==4 && this.status == 200) {
      console.table(JSON.parse(request.responseText).value);
      productosObtenidos = request.responseText;
      procesarProductos();
   }
  }
  request.open("GET", url, true);
  request.send();
}

function procesarProductos() {
  var JSONProductos = JSON.parse(productosObtenidos);
  //alert(JSONProductos.value[0].ProductName);
  var divTabla = document.getElementById("divTabla");
  var tabla = document.createElement("table");
  var tbody = document.createElement("tbody");

  tabla.classList.add("table");
  tabla.classList.add("table-stripped");

  for (var index = 0; index < JSONProductos.length; index++) {

    var nuevaFila = document.createElement("tr");

    var columnaNombre = document.createElement("td");
    columnaNombre.innerText = JSONProductos.value[index].ProductName;

    var columnaPrecio = document.createElement("td");
    columnaNombre.innerText = JSONProductos.value[index].UnitPrice;

    var columnaStock = document.createElement("td");
    columnaNombre.innerText = JSONProductos.value[index].UnitsInStock;

    nuevaFila.appendChild(columnaNombre);
    nuevaFila.appendChild(columnaPrecio);
    nuevaFila.appendChild(columnaStock);

    tbody.appendChild(nuevaFila);
  }

  tabla.appendChild(tbody);
  divTabla.appendChild(tabla);
}
